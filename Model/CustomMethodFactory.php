<?php


namespace Bss\CustomShippingMethod\Model;

/**
 * Class CustomMethodFactory
 * Parses `return` annotation from doc block.
 */
class CustomMethodFactory
{

    private $rawData;
    /**
     *
     */
    public function create()
    {
        return $this;
    }

    /**
     * @param int $rowId
     * @return int
     */
    public function load(int $rowId)
    {
        if ($rowId) {
            return $this->rawData;
        }
        return "row Id not found";
    }

    /**
     * @return string name
     */
    public function getName()
    {
        return $this->rawData->name;
    }

    /**
     * @return string name
     */
    public function getEntityId()
    {
        return $this->rawData->entityId;
    }
}
