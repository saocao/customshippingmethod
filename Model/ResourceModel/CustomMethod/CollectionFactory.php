<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CustomShippingMethod
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CustomShippingMethod\Model\ResourceModel\CustomMethod;

/**
 * Class CollectionFactory
 *
 * CollectionFactory create
 */
class CollectionFactory extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * CollectionFactory constructor.
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    // phpcs:disable Generic.CodeAnalysis.UselessOverridingMethod
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('bss_custom_shipping_method', 'id');
    }

    /**
     *
     */
    public function create()
    {
        return $this;
    }
}
